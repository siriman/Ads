package ads.dnr2i.m2.info.unicaen.fr.ads.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Classe representant une annonce.
 */
public class Annonce {

    @SerializedName("id")
    protected String id;

    @SerializedName("titre")
    protected String titre;

    @SerializedName("description")
    protected String description;

    @SerializedName("prix")
    protected String prix;

    @SerializedName("emailContact")
    protected String emailContact;

    @SerializedName("telContact")
    protected String telContact;

    @SerializedName("ville")
    protected String ville;

    @SerializedName("cp")
    protected String cp;

    @SerializedName("pseudo")
    protected String pseudo;

    @SerializedName("images")
    protected List<String> images;

    public Annonce() {
    }

    public Annonce(String id, String titre, String description, String prix, String email, String contact,
                   String ville, String cp, String pseudo, List<String> images) {

        this.id = id;
        this.titre = titre;
        this.description = description;
        this.prix = prix;
        this.emailContact = email;
        this.telContact = contact;
        this.ville = ville;
        this.cp = cp;
        this.pseudo = pseudo;
        this.images = images;
    }

    public Annonce(String titre) {
        this.titre = titre;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrix() {
        return prix;
    }

    public void setPrix(String prix) {
        this.prix = prix;
    }

    public String getEmailContact() {
        return emailContact;
    }

    public void setEmailContact(String emailContact) {
        this.emailContact = emailContact;
    }

    public String getTelContact() {
        return telContact;
    }

    public void setTelContact(String telContact) {
        this.telContact = telContact;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    @Override
    public String toString() {
        return "Annonce{" +
                "id='" + id + '\'' +
                ", titre='" + titre + '\'' +
                ", description='" + description + '\'' +
                ", prix='" + prix + '\'' +
                ", emailContact='" + emailContact + '\'' +
                ", telContact='" + telContact + '\'' +
                ", ville='" + ville + '\'' +
                ", cp='" + cp + '\'' +
                ", pseudo='" + pseudo + '\'' +
                ", images=" + images +
                '}';
    }
}
