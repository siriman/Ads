package ads.dnr2i.m2.info.unicaen.fr.ads.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ads.dnr2i.m2.info.unicaen.fr.ads.R;
import ads.dnr2i.m2.info.unicaen.fr.ads.adapters.AnnonceAdapter;
import ads.dnr2i.m2.info.unicaen.fr.ads.models.Annonce;

/**
 * Actitity representant la page liste des annonces.
 */
public class AdsActivity extends AppCompatActivity {

    /**
     * Liste des annonces
     */
    protected List<Annonce> annonces;

    /**
     * RecyclerView
     */
    protected RecyclerView annonceRecycleView;

    /**
     * Adaptateur des annonces
     */
    protected AnnonceAdapter annonceAdapter;

    /**
     * Gson
     */
    private Gson gson;

    /**
     * Lien
     */
    final String url = "https://ensweb.users.info.unicaen.fr/android-api/?apikey=dnr3&method=listAll";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ads);

        annonces = new ArrayList<>();
        GsonBuilder gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest jsonRequest = new StringRequest(Request.Method.GET, url,
                new GetAnnoncesResponse(), // la classe interne qui sera appelée quand on reçoit la réponse
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(AdsActivity.this, R.string.tost_internet_error, Toast.LENGTH_LONG).show();
                    }
                }
        );
        queue.add(jsonRequest);
    }

    /**
     * Charge les donnees dans le RecyclerView
     */
    public void loading(){
        annonceAdapter = new AnnonceAdapter(annonces);
        annonceRecycleView = (RecyclerView) findViewById(R.id.annonce_recycle_view);
        annonceRecycleView.setLayoutManager(new LinearLayoutManager(this));
        annonceRecycleView.setAdapter(annonceAdapter);
        annonceRecycleView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {

            GestureDetector gestureDetector = new GestureDetector(AdsActivity.this, new GestureDetector.SimpleOnGestureListener() {
                @Override public boolean onSingleTapUp(MotionEvent motionEvent) {
                    return true;
                }
            });

            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                View view = rv.findChildViewUnder(e.getX(), e.getY());
                if(view != null && gestureDetector.onTouchEvent(e)) {
                    int position = rv.getChildAdapterPosition(view);

                    Annonce annonce = annonceAdapter.getItem(position);
                    Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
                    intent.putExtra("annonce_id", annonce.getId());
                    intent.putExtra("destinationValue", false);
                    finish();
                    startActivity(intent);
                }

                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
    }

    /**
     * Gestionnaire de la reponse de la requette HTTP
     */
    public class GetAnnoncesResponse implements Response.Listener<String> {

        @Override
        public void onResponse(String response) {
            if(response != null){
                try {
                    JSONObject data = new JSONObject(response);
                    if(data.getBoolean("success")){
                        annonces = Arrays.asList(gson.fromJson(data.getString("response"), Annonce[].class));
                        //System.out.println("ANNONCES ================"+annonces);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            loading();
        }
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        finish();
        startActivity(intent);
    }
}
