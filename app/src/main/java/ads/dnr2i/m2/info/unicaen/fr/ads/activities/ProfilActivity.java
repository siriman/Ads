package ads.dnr2i.m2.info.unicaen.fr.ads.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import ads.dnr2i.m2.info.unicaen.fr.ads.R;

/**
 * Activty representant le profil
 */
public class ProfilActivity extends AppCompatActivity {

    /**
     * EditText pseudo
     */
    EditText pseudoEditText;

    /**
     * EditText mail
     */
    EditText mailEditText;

    /**
     * EditText telephone
     */
    EditText telEditText;

    /**
     * EditText code postal
     */
    EditText cpEditText;

    /**
     * EditText ville
     */
    EditText villeEditText;

    /**
     * Bouton de validation
     */
    Button validerButton;

    TextInputLayout inputLayoutPseudo;
    TextInputLayout inputLayoutMail;
    TextInputLayout inputLayoutTel;
    TextInputLayout inputLayoutCp;
    TextInputLayout inputLayoutVille;
    SharedPreferences preferences;

    //CLES UTILISEES DANS LES PREFERENCES
    public static final String KEY_PSEUDO = "PSEUDO";
    public static final String KEY_MAIL   = "MAIL";
    public static final String KEY_TEL    = "TEL";
    public static final String KEY_CP     = "CP";
    public static final String KEY_VILLE  = "VILLE";
    public static final String KEY_PREF   = "DATA_USER";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);
        preferences = getSharedPreferences(KEY_PREF, MODE_PRIVATE);

        pseudoEditText  = findViewById(R.id.profil_pseudo);
        mailEditText    = findViewById(R.id.profil_email);
        telEditText     = findViewById(R.id.profil_tel);
        cpEditText      = findViewById(R.id.profil_cp);
        villeEditText   = findViewById(R.id.profil_ville);

        inputLayoutPseudo   = findViewById(R.id.input_layout_pseudo);
        inputLayoutMail     = findViewById(R.id.input_layout_mail);
        inputLayoutTel      = findViewById(R.id.input_layout_tel);
        inputLayoutCp       = findViewById(R.id.input_layout_cp);
        inputLayoutVille    = findViewById(R.id.input_layout_ville);

        validerButton   = findViewById(R.id.button_validation);

        pseudoEditText.setText(preferences.getString(KEY_PSEUDO, ""));
        mailEditText.setText(preferences.getString(KEY_MAIL, ""));
        telEditText.setText(preferences.getString(KEY_TEL, ""));
        cpEditText.setText(preferences.getString(KEY_CP, ""));
        villeEditText.setText(preferences.getString(KEY_VILLE, ""));
    }


    public void valider(View view){
        if(!checkField())
            return;

        String pseudo   = pseudoEditText.getText().toString();
        String mail     = mailEditText.getText().toString();
        String tel      = telEditText.getText().toString();
        String cp       = cpEditText.getText().toString();
        String ville    = villeEditText.getText().toString();

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_PSEUDO, pseudo);
        editor.putString(KEY_MAIL, mail);
        editor.putString(KEY_TEL, tel);
        editor.putString(KEY_CP, cp);
        editor.putString(KEY_VILLE, ville);
        editor.commit();

        Intent intent = new Intent(this, MainActivity.class);
        finish();
        startActivity(intent);
    }

    private boolean checkField(){
        if (!validatePseudo()) {
            return false;
        }
        if (!validateEmail()) {
            return false;
        }
        if (!validateTel()) {
            return false;
        }

        if (!validateCp()) {
            return false;
        }

        if (!validateVille()) {
            return false;
        }
        return true;
    }

    private boolean validatePseudo() {
        if (pseudoEditText.getText().toString().trim().isEmpty()) {
            pseudoEditText.setError(getString(R.string.err_msg_name));
            requestFocus(pseudoEditText);
            return false;
        }
        inputLayoutPseudo.setErrorEnabled(false);
        return true;
    }

    private boolean validateEmail() {
        String email = mailEditText.getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            inputLayoutMail.setError(getString(R.string.err_msg_mail));
            requestFocus(mailEditText);
            return false;
        }
        inputLayoutMail.setErrorEnabled(false);
        return true;
    }

    private boolean validateTel() {
        if (telEditText.getText().toString().trim().isEmpty()) {
            telEditText.setError(getString(R.string.err_msg_tel));
            requestFocus(telEditText);
            return false;
        }

        if ( telEditText.getText().toString().trim().length() != 10 ) {
            telEditText.setError(getString(R.string.err_msg_tel));
            requestFocus(telEditText);
            return false;
        }

        inputLayoutTel.setErrorEnabled(false);
        return true;
    }


    private boolean validateCp() {
        if (cpEditText.getText().toString().trim().isEmpty()) {
            cpEditText.setError(getString(R.string.err_msg_cp));
            requestFocus(cpEditText);
            return false;
        }

        if ( cpEditText.getText().toString().trim().length() != 5 ) {
            cpEditText.setError(getString(R.string.err_msg_cp));
            requestFocus(cpEditText);
            return false;
        }

        inputLayoutCp.setErrorEnabled(false);
        return true;
    }

    private boolean validateVille() {
        if (villeEditText.getText().toString().trim().isEmpty()) {
            villeEditText.setError(getString(R.string.err_msg_ville));
            requestFocus(villeEditText);
            return false;
        }

        inputLayoutVille.setErrorEnabled(false);
        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private class ProfilTextWatcher implements TextWatcher {

        private View view;

        private ProfilTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.profil_pseudo:
                    validatePseudo();
                    break;
                case R.id.profil_email:
                    validateEmail();
                    break;
                case R.id.profil_tel:
                    validateTel();
                    break;
                case R.id.profil_cp:
                    validateCp();
                    break;
                case R.id.profil_ville:
                    validateVille();
                    break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        finish();
        startActivity(intent);
    }
}
