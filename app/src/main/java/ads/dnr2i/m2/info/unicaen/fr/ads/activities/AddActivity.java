package ads.dnr2i.m2.info.unicaen.fr.ads.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import ads.dnr2i.m2.info.unicaen.fr.ads.R;
import ads.dnr2i.m2.info.unicaen.fr.ads.models.Annonce;

import static ads.dnr2i.m2.info.unicaen.fr.ads.activities.ProfilActivity.KEY_CP;
import static ads.dnr2i.m2.info.unicaen.fr.ads.activities.ProfilActivity.KEY_MAIL;
import static ads.dnr2i.m2.info.unicaen.fr.ads.activities.ProfilActivity.KEY_PREF;
import static ads.dnr2i.m2.info.unicaen.fr.ads.activities.ProfilActivity.KEY_PSEUDO;
import static ads.dnr2i.m2.info.unicaen.fr.ads.activities.ProfilActivity.KEY_TEL;
import static ads.dnr2i.m2.info.unicaen.fr.ads.activities.ProfilActivity.KEY_VILLE;

public class AddActivity extends AppCompatActivity {

    protected ArrayList<Annonce> annonces = new ArrayList<Annonce>();
    ;
    protected ListView annoncesListView;

    EditText villeField;
    EditText titreField;
    EditText descriptionField;
    EditText prixField;
    EditText pseudoField;
    EditText emailField;
    EditText telephoneField;
    EditText cpField;
    String ville;
    String titre;
    String description;
    String prix;
    String pseudo;
    String email;
    String telephone;
    String cp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        titreField = (EditText) findViewById(R.id.EditTextTitre);
        descriptionField = (EditText) findViewById(R.id.EditTextDescription);
        prixField = (EditText) findViewById(R.id.EditTextPrix);
        pseudoField = (EditText) findViewById(R.id.EditTextPseudo);
        emailField = (EditText) findViewById(R.id.EditTextEmail);
        telephoneField = (EditText) findViewById(R.id.EditTextTelephone);
        cpField = (EditText) findViewById(R.id.EditTextCp);
        villeField = (EditText) findViewById(R.id.EditTextVille);

        SharedPreferences preferences = getSharedPreferences(KEY_PREF, MODE_PRIVATE);
        pseudoField.setText(preferences.getString(KEY_PSEUDO, ""));
        emailField.setText(preferences.getString(KEY_MAIL, ""));
        telephoneField.setText(preferences.getString(KEY_TEL, ""));
        cpField.setText(preferences.getString(KEY_CP, ""));
        villeField.setText(preferences.getString(KEY_VILLE, ""));
    }

    public void sendFeedback(View button) {
        titre = titreField.getText().toString();
        description = descriptionField.getText().toString();
        prix = prixField.getText().toString();
        pseudo = pseudoField.getText().toString();
        email = emailField.getText().toString();
        telephone = telephoneField.getText().toString();
        cp = cpField.getText().toString();
        ville = villeField.getText().toString();

        if (!validation())
            return;

        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "https://ensweb.users.info.unicaen.fr/android-api/";
        // Request a string response from the provided URL.
        StringRequest jsonRequest = new StringRequest(Request.Method.POST, url,
                new AddActivity.MyAddResponse(), // la classe interne qui sera appelée quand on reçoit la réponse
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(AddActivity.this, R.string.tost_internet_error, Toast.LENGTH_LONG).show();
                        Log.v("INFO :", "L'enregistrement a échoué");
                    }

                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("apikey", "dnr3");
                params.put("method", "save");
                params.put("ville", ville);
                params.put("titre", titre);
                params.put("description", description);
                params.put("prix", prix);
                params.put("pseudo", pseudo);
                params.put("emailContact", email);
                params.put("telContact", telephone);
                params.put("cp", cp);

                return params;
            }

        };

        // Add the request to the RequestQueue.
        queue.add(jsonRequest);
    }

    private class MyAddResponse implements Response.Listener<String> {
        @Override
        public void onResponse(String response) {
            System.out.println("---------------------------------------------- Réponse de test : " +response+" --");
            try {
                JSONObject jsonObject = new JSONObject(response);
                boolean status = jsonObject.getBoolean("success");
                if(status){
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    Annonce annonce = gson.fromJson(jsonObject.getString("response"), Annonce.class);
                    Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
                    intent.putExtra("annonce_id", annonce.getId());
                    finish();
                    startActivity(intent);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private boolean validation() {

        if (ville == null) {
            System.out.println("---------------------------------------------- ville --");
            return false;
        }
        if(titre == null) {
            System.out.println("---------------------------------------------- titre --");
            return false;
        }
        if (description == null) {
            System.out.println("---------------------------------------------- description --");
            return false;
        }
        if (prix == null) {
            System.out.println("---------------------------------------------- prix --");
            return false;
        }
        if (pseudo == null) {
            System.out.println("---------------------------------------------- pseudo --");
            return false;
        }
        if (!isEmailValid(email)) {
            System.out.println("---------------------------------------------- email --");
            return false;
        }
        if (telephone == null) {
            System.out.println("---------------------------------------------- tel--");
            return false;
        }
        if (cp == null) {
            System.out.println("---------------------------------------------- cp --");
            return false;
        }

        System.out.println("---------------------------------------------- Tout va bien --");
        return true;
    }

    boolean isEmailValid(CharSequence email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        finish();
        startActivity(intent);
    }
}
