package ads.dnr2i.m2.info.unicaen.fr.ads.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ads.dnr2i.m2.info.unicaen.fr.ads.R;
import ads.dnr2i.m2.info.unicaen.fr.ads.adapters.*;
import ads.dnr2i.m2.info.unicaen.fr.ads.models.Annonce;

import android.support.v4.view.ViewPager;

public class DetailActivity extends Activity {
    private Gson gson;
    protected List<Annonce> annonces = new ArrayList<Annonce>();
    private String url = "https://ensweb.users.info.unicaen.fr/android-api/?apikey=dnr3&method=details&id=";
    private String annonceId;
    ImageButton btnCall;
    ImageButton btnEmail;
    Annonce annonce;
    Boolean destination;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        annonceId = getIntent().getExtras().getString("annonce_id");
        destination = getIntent().getExtras().getBoolean("destinationValue");
        annonces = new ArrayList<>();
        GsonBuilder gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest jsonRequest = new StringRequest(Request.Method.GET, url + annonceId,
                new DetailActivity.GetAnnoncesResponse(), // la classe interne qui sera appelée quand on reçoit la réponse
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("ERROR  ===================>" + error);
                    }
                }
        );
        // Add the request to the RequestQueue.
        queue.add(jsonRequest);


        //Action button
        btnCall = findViewById(R.id.btn_call);
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String numberAnnonce = annonce.getTelContact();
                Uri number = Uri.parse("tel:" + numberAnnonce);
                Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
                startActivity(Intent.createChooser(callIntent, "Choisir une application pour téléphoner :"));
            }
        });

        btnEmail = findViewById(R.id.btn_email);
        btnEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String mailTo = annonce.getEmailContact();
                System.out.println("========> " + mailTo);
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + mailTo));
                startActivity(Intent.createChooser(emailIntent, "Choisir une application de envoyer un mail :"));
            }
        });
    }

    public void load(Annonce annonce) {
        TextView titre = findViewById(R.id.titre);
        titre.setText(annonce.getTitre().toUpperCase());

        TextView description = findViewById(R.id.description_text);
        description.setText(annonce.getDescription());

        TextView prix = findViewById(R.id.prix_text);
        prix.setText(annonce.getPrix()+" €");

        TextView ville = findViewById(R.id.ville_text);
        ville.setText(annonce.getVille());

        TextView codePostal = findViewById(R.id.cp_text);
        codePostal.setText(annonce.getCp());

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, AdsActivity.class);
        if (destination)
            intent = new Intent(this, SearchActivity.class);
        finish();
        startActivity(intent);
    }

    private class GetAnnoncesResponse implements Response.Listener<String> {

        @Override
        public void onResponse(String response) {
            if (response != null) {
                try {
                    JSONObject data = new JSONObject(response);
                    if (data.getBoolean("success")) {
                        annonces = Arrays.asList(gson.fromJson(data.getString("response"), Annonce.class));
                        annonce = annonces.get(0);
                        load(annonce);

                        ViewPager mViewPager = (ViewPager) findViewById(R.id.viewPageAndroid);
                        ImageAdapter adapterView = new ImageAdapter(getApplicationContext());
                        adapterView.setSliderImagesId(annonce.getImages());
                        mViewPager.setAdapter(adapterView);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            //loadAnnonces();
        }
    }
}
