package ads.dnr2i.m2.info.unicaen.fr.ads.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import ads.dnr2i.m2.info.unicaen.fr.ads.R;
import ads.dnr2i.m2.info.unicaen.fr.ads.models.Annonce;

/**
 * Classe represnentant un item du RecyclerView
 */
public class AdsHolder extends RecyclerView.ViewHolder {

    /**
     * Widget du titre
     */
    TextView titre;

    /**
     * Widget de la reference.
     */
    TextView prix;


    /**
     * Widget du telContact.
     */
    TextView contact;

    /**
     * Widget de l'icon.
     */
    ImageView image;

    /**
     * Constructeur logique
     *
     * @param itemView un widget
     */
    public AdsHolder(View itemView) {
        super(itemView);
        this.image = itemView.findViewById(R.id.ads_image);
        this.titre = (TextView)itemView.findViewById(R.id.ads_title);
        this.prix = (TextView)itemView.findViewById(R.id.ads_price);
        this.contact = itemView.findViewById(R.id.ads_contact);
    }

    /**
     * Charge les contenues
     *
     * @param annonce une annonce
     */
    public void bind(Annonce annonce){
        String url = annonce.getImages().size() > 0 ? annonce.getImages().get(0) : "";
        Picasso.with(image.getContext()).load(url).centerCrop().fit().into(image);
        titre.setText(annonce.getTitre().toUpperCase());
        contact.setText("postée par : "+annonce.getPseudo());
        prix.setText(annonce.getPrix()+"€");
    }
}
