package ads.dnr2i.m2.info.unicaen.fr.ads.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ads.dnr2i.m2.info.unicaen.fr.ads.R;
import ads.dnr2i.m2.info.unicaen.fr.ads.models.Annonce;

/**
 * Created by siriman on 22/01/18.
 */

public class AnnonceAdapter extends RecyclerView.Adapter<AdsHolder> {

    private List<Annonce> annonces;

    public AnnonceAdapter(List<Annonce> annonces){
        this.annonces = annonces;
    }

    @Override
    public AdsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ads_card, parent,false);
        return new AdsHolder(view);
    }

    @Override
    public void onBindViewHolder(AdsHolder holder, int position) {
        Annonce annonce = annonces.get(position);
        holder.bind(annonce);
    }

    @Override
    public int getItemCount() {
        return annonces.size();
    }

    public Annonce getItem(int id){
        return annonces.get(id);
    }
}
