package ads.dnr2i.m2.info.unicaen.fr.ads.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ads.dnr2i.m2.info.unicaen.fr.ads.R;
import ads.dnr2i.m2.info.unicaen.fr.ads.adapters.AnnonceAdapter;
import ads.dnr2i.m2.info.unicaen.fr.ads.models.Annonce;

public class SearchActivity extends AppCompatActivity {
    protected List<Annonce> annonces = new ArrayList<Annonce>();;
    protected RecyclerView annonceRecycleView;
    protected AnnonceAdapter annonceAdapter;
    private Gson gson;
    final String url = "https://ensweb.users.info.unicaen.fr/android-api/?apikey=dnr3&method=listAll";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SearchView simpleSearchView=(SearchView)findViewById(R.id.search);

        simpleSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                loading(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                loading(newText);
                return false;
            }
        });


        annonces = new ArrayList();
        GsonBuilder gsonBuilder = new GsonBuilder();
//        gsonBuilder.setDateFormat("M/d/yy hh:mm a");
        gson = gsonBuilder.create();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest jsonRequest = new StringRequest(Request.Method.GET, url,
                new SearchActivity.GetAnnoncesResponse(), // la classe interne qui sera appelée quand on reçoit la réponse
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("ERROR  ===================>"+error);
                    }
                }
        );
        // Add the request to the RequestQueue.
        queue.add(jsonRequest);
    }

    private class GetAnnoncesResponse implements Response.Listener<String> {

        @Override
        public void onResponse(String response) {
            if(response != null) {
                try {
                    JSONObject data = new JSONObject( response );
                    if (data.getBoolean( "success" )) {
                        annonces = Arrays.asList( gson.fromJson( data.getString( "response" ), Annonce[].class ) );
                        System.out.println( "ANNONCES ================" + data.getString( "response" ) );
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public void loading(String query){
        TextView t1 = (TextView) findViewById(R.id.noresulttxt);
        String search = "";
        ArrayList copie = new ArrayList(annonces);
        for (int i = 0 ;  i < copie.size() ; i++) {
            search = annonces.get(i).getTitre().toLowerCase();
            if (!search.equals(query.toLowerCase())) {
                if (copie.size() > 1) {
                    copie.remove( i );
                } else {
                    copie = new ArrayList();
                }
            }
        }
        annonceRecycleView = (RecyclerView) findViewById( R.id.searchview );
        annonceRecycleView.setLayoutManager( new LinearLayoutManager( this ) );
        if (copie.size() >= 1) {
            t1.setText("");
            annonceAdapter = new AnnonceAdapter( copie );
            annonceRecycleView.setAdapter( annonceAdapter );
        } else {
            annonceAdapter = new AnnonceAdapter( copie );
            annonceRecycleView.setAdapter( annonceAdapter );
            t1.setText("Pas de résultats trouvés pour la recherche : "+query.toLowerCase()+"");
        }

        annonceRecycleView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {

            GestureDetector gestureDetector = new GestureDetector(SearchActivity.this, new GestureDetector.SimpleOnGestureListener() {
                @Override public boolean onSingleTapUp(MotionEvent motionEvent) {
                    return true;
                }
            });

            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                View view = rv.findChildViewUnder(e.getX(), e.getY());
                if(view != null && gestureDetector.onTouchEvent(e)) {
                    int position = rv.getChildAdapterPosition(view);

                    Annonce annonce = annonceAdapter.getItem(position);
                    Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
                    intent.putExtra("annonce_id", annonce.getId());
                    intent.putExtra("destinationValue", true);
                    finish();
                    startActivity(intent);
                }

                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        finish();
        startActivity(intent);
    }
}
