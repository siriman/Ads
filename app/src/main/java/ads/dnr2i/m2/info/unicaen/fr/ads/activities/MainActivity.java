package ads.dnr2i.m2.info.unicaen.fr.ads.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import ads.dnr2i.m2.info.unicaen.fr.ads.R;

import static ads.dnr2i.m2.info.unicaen.fr.ads.activities.ProfilActivity.KEY_PREF;
import static ads.dnr2i.m2.info.unicaen.fr.ads.activities.ProfilActivity.KEY_PSEUDO;

/**
 * Activity representant l'ecran accueil
 */
public class MainActivity extends AppCompatActivity {

    Button accueil, deposer, rechercher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/
        //--------- LOGO
        //getSupportActionBar().setDisplayShowHomeEnabled(true);
//        getSupportActionBar().setIcon(R.drawable.logo);

        accueil     = (Button) findViewById(R.id.accueil);
        deposer     = (Button) findViewById(R.id.deposer);
        rechercher  = (Button) findViewById(R.id.rechercher);

        SharedPreferences preferences = getSharedPreferences(KEY_PREF, MODE_PRIVATE);
        if(preferences.getString(KEY_PSEUDO, "").equals("")){
            showAlert();
            return;
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "", Snackbar.LENGTH_LONG)
                        .setAction("Éditer votre profil", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                openProfilActivity();
                            }
                        }).show();
            }
        });
    }

    /**
     * Affiche une alerte
     */
    public void showAlert(){
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle(R.string.creer_pseudo);
        alertDialog.setCancelable(false);
        alertDialog.setMessage(getResources().getString(R.string.message_creer_profil));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.btn_ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        openProfilActivity();
                    }
                });
        alertDialog.show();
    }

    /**
     * Ouvre l'activity profil
     */
    private void openProfilActivity(){
        Intent intent = new Intent(getApplicationContext(), ProfilActivity.class);
        finish();
        startActivity(intent);
    }

    /**
     * Gestion des click des boutons
     * @param view button
     */
    public void openView(View view){
        Intent intent = null;
        if(view == accueil ){
            intent = new Intent(this, AdsActivity.class);
        }

        if(view == deposer ){
            intent = new Intent(this, AddActivity.class);
        }

        if(view == rechercher ){
            intent = new Intent(this, SearchActivity.class);
        }

        finish();
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
